# cinit

Automatically create CMake projects

| :exclamation: This project is unstable and may behave unexpectedly or cause system damage |
|-------------------------------------------------------------------------------------------|

### Syntax
Go to the directory you would like to create the project in then run:

> cinit <i>project-name</i>

This will create your project.
Optionally add `--no-vcs` to the end to disable the automatic creation of a repository using a version control system (such as git). For a full list of options run `cinit --help`.

## Installation

### Arch Linux
Clone this repository and run `makepkg -si` from `install/archlinux`

### Other Linux
Clone this repository and run `install.sh` from `install/otherlinux`

## Contributing

Feel free to create issues and pull requests but make sure to be <b>clear and concise</b> with your explainations.
