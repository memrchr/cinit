#!/usr/bin/python

import sys, os, pexpect

template_path = os.path.dirname(os.path.abspath(__file__))
target_path = sys.argv[1]
project_name = sys.argv[2]

try:
    os.system(f"cp -r {template_path}/* {target_path}/{project_name}")
except:
    quit(1)

def vcs():
    vcsignore = open(f"{target_path}/.gitignore", "w")
    vcsignore.write("bin/")
    vcsignore.close()

def set_attribs():
    cmake = open(f"{template_path}/CMakeLists.txt", "r")
    cmake_lines = cmake.readlines()

    cmake_lines[1] = f"project({project_name})"
    cmake_lines[5] = f"add_executable({project_name} " + "${sources})"

    cmake_rename = open(f"{target_path}/{project_name}/CMakeLists.txt", "w")
    cmake_rename.writelines(cmake_lines)

    cmake.close()
    cmake_rename.close()
 
    make = pexpect.spawn("/usr/bin/make", cwd="{}/{}".format(os.getcwd(), project_name))
    make.interact()

if 4 > len(sys.argv):
    set_attribs()
    vcs()
else:
    if sys.argv[3] == "bare":
        quit()
    if sys.argv[3] == "barevcs":
        vcs()
        quit()
    if sys.argv[3] == "novcs":
        set_attribs()
        quit()

